"""
Helper function to read data files
"""

import h5py
import json
import numpy as np
import os

from .context import PROJ_DIR


def load_spectra(use_cleaned_data=False):

    if use_cleaned_data:
        fp_h5 = os.path.join(
            PROJ_DIR, 'data', 'processed', 'C1554970778_1_cleaned.h5'
        )
    else:
        fp_h5 = os.path.join(
            PROJ_DIR, 'data', 'converted', 'C1554970778_1.h5'
        )
    
    with h5py.File(fp_h5, 'r') as h5:
        spectra = h5['spectra'][:]
        wavelength = h5['wavelengths'][:]

    return wavelength, spectra
