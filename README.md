# Sample project

Author: N. Luettschwager

Start date: 2019-09-26

Issued: 2021-03-19

## Introduction

This sample project is part of the course [Data Evaluation with Python: Tools and Good Practice](https://hbond.uni-goettingen.de/ma_chem/epc/mc.html#dep).
The goal of this project is to show how to organise Python code
and Jupyter notebooks in a way that is easy to understand by others and easy
to install and run. It includes several common files (such as `requirements.txt`,
`README.md`, `LICENSE`) and tries to follow good practice recommendations closely to facilitate reproducability and to enable easy sharing.

In the project, we analyze data from the Cassini mission. Spatially resolved spectra
of the Saturn moon [Titan](https://en.wikipedia.org/wiki/Titan_%28moon%29), recorded using the [VIMS](https://solarsystem.nasa.gov/missions/cassini/mission/spacecraft/cassini-orbiter/visible-and-infrared-mapping-spectrometer/)
instrument, will be analysed and we will show that we can seperate spectra characteristic of the atmosphere from spectra characteristic for light reflected from the surface by statistical analysis.

By doing this we will touch the following topics:

- file I/O with files in the HDF5 format, hashing files
  ([notebooks/0_convert_VIMS_files.ipynb](notebooks/0_convert_VIMS_files.ipynb))
- interactive data visualization using
  [Bokeh](https://docs.bokeh.org/en/latest/)
  and [widgets](https://ipywidgets.readthedocs.io/en/stable/)
  ([notebooks/1_data_exploration.ipynb](notebooks/1_data_exploration.ipynb))
- finding outliers in the data by statistical means using [Numpy](https://numpy.org/)
  ([notebooks/2_preprocessing.ipynb](notebooks/2_preprocessing.ipynb))
- finding different types of spectra using statistical tools from the
  [scikit-learn package](https://scikit-learn.org/stable/index.html)
  (k-means clustering, principle component analysis,
  [notebooks/3_classification.ipynb](notebooks/3_classification.ipynb))
- writing a report in Jupyter and exporting it to Word or LaTeX using
  [nbconvert](https://nbconvert.readthedocs.io/en/latest/) and
  [pandoc](https://pandoc.org)
  ([notebooks/4_report.ipynb](notebooks/4_report.ipynb))

Note that while this project shall serve as an example how a "real" research project based on Python/Jupyter may be organized, the notebooks are written in an "educational style", meaning that in a research project there would be more comments on the actual data than on the mechanics of programming.

In the course, we will not go through the code in great detail (you can read through it and send me some questions), but are more concerned with how the files are organized and certain things are achieved, e.g.:

- sharing code among notebooks by using a custom module (`notebooks/scripts`)
- defining short code snippets that can be loaded into a notebook (`notebooks/snippets`)
- testing code and notebooks (`test`)

If you have ideas on how to improve the sample project, please [open an issue on gitlab.gwdg.de](https://gitlab.gwdg.de/jupyter-course/sample-project/-/issues).

## Running on Binder

The easiest way to explore this project is to run it on [mybinder.org](https://mybinder.org).
No installation or dependency handling required.

To do so, click one of these badges:

[![badge](https://img.shields.io/badge/launch-binder%20(classic%20notebook)-579ACA.svg?logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFkAAABZCAMAAABi1XidAAAB8lBMVEX///9XmsrmZYH1olJXmsr1olJXmsrmZYH1olJXmsr1olJXmsrmZYH1olL1olJXmsr1olJXmsrmZYH1olL1olJXmsrmZYH1olJXmsr1olL1olJXmsrmZYH1olL1olJXmsrmZYH1olL1olL0nFf1olJXmsrmZYH1olJXmsq8dZb1olJXmsrmZYH1olJXmspXmspXmsr1olL1olJXmsrmZYH1olJXmsr1olL1olJXmsrmZYH1olL1olLeaIVXmsrmZYH1olL1olL1olJXmsrmZYH1olLna31Xmsr1olJXmsr1olJXmsrmZYH1olLqoVr1olJXmsr1olJXmsrmZYH1olL1olKkfaPobXvviGabgadXmsqThKuofKHmZ4Dobnr1olJXmsr1olJXmspXmsr1olJXmsrfZ4TuhWn1olL1olJXmsqBi7X1olJXmspZmslbmMhbmsdemsVfl8ZgmsNim8Jpk8F0m7R4m7F5nLB6jbh7jbiDirOEibOGnKaMhq+PnaCVg6qWg6qegKaff6WhnpKofKGtnomxeZy3noG6dZi+n3vCcpPDcpPGn3bLb4/Mb47UbIrVa4rYoGjdaIbeaIXhoWHmZYHobXvpcHjqdHXreHLroVrsfG/uhGnuh2bwj2Hxk17yl1vzmljzm1j0nlX1olL3AJXWAAAAbXRSTlMAEBAQHx8gICAuLjAwMDw9PUBAQEpQUFBXV1hgYGBkcHBwcXl8gICAgoiIkJCQlJicnJ2goKCmqK+wsLC4usDAwMjP0NDQ1NbW3Nzg4ODi5+3v8PDw8/T09PX29vb39/f5+fr7+/z8/Pz9/v7+zczCxgAABC5JREFUeAHN1ul3k0UUBvCb1CTVpmpaitAGSLSpSuKCLWpbTKNJFGlcSMAFF63iUmRccNG6gLbuxkXU66JAUef/9LSpmXnyLr3T5AO/rzl5zj137p136BISy44fKJXuGN/d19PUfYeO67Znqtf2KH33Id1psXoFdW30sPZ1sMvs2D060AHqws4FHeJojLZqnw53cmfvg+XR8mC0OEjuxrXEkX5ydeVJLVIlV0e10PXk5k7dYeHu7Cj1j+49uKg7uLU61tGLw1lq27ugQYlclHC4bgv7VQ+TAyj5Zc/UjsPvs1sd5cWryWObtvWT2EPa4rtnWW3JkpjggEpbOsPr7F7EyNewtpBIslA7p43HCsnwooXTEc3UmPmCNn5lrqTJxy6nRmcavGZVt/3Da2pD5NHvsOHJCrdc1G2r3DITpU7yic7w/7Rxnjc0kt5GC4djiv2Sz3Fb2iEZg41/ddsFDoyuYrIkmFehz0HR2thPgQqMyQYb2OtB0WxsZ3BeG3+wpRb1vzl2UYBog8FfGhttFKjtAclnZYrRo9ryG9uG/FZQU4AEg8ZE9LjGMzTmqKXPLnlWVnIlQQTvxJf8ip7VgjZjyVPrjw1te5otM7RmP7xm+sK2Gv9I8Gi++BRbEkR9EBw8zRUcKxwp73xkaLiqQb+kGduJTNHG72zcW9LoJgqQxpP3/Tj//c3yB0tqzaml05/+orHLksVO+95kX7/7qgJvnjlrfr2Ggsyx0eoy9uPzN5SPd86aXggOsEKW2Prz7du3VID3/tzs/sSRs2w7ovVHKtjrX2pd7ZMlTxAYfBAL9jiDwfLkq55Tm7ifhMlTGPyCAs7RFRhn47JnlcB9RM5T97ASuZXIcVNuUDIndpDbdsfrqsOppeXl5Y+XVKdjFCTh+zGaVuj0d9zy05PPK3QzBamxdwtTCrzyg/2Rvf2EstUjordGwa/kx9mSJLr8mLLtCW8HHGJc2R5hS219IiF6PnTusOqcMl57gm0Z8kanKMAQg0qSyuZfn7zItsbGyO9QlnxY0eCuD1XL2ys/MsrQhltE7Ug0uFOzufJFE2PxBo/YAx8XPPdDwWN0MrDRYIZF0mSMKCNHgaIVFoBbNoLJ7tEQDKxGF0kcLQimojCZopv0OkNOyWCCg9XMVAi7ARJzQdM2QUh0gmBozjc3Skg6dSBRqDGYSUOu66Zg+I2fNZs/M3/f/Grl/XnyF1Gw3VKCez0PN5IUfFLqvgUN4C0qNqYs5YhPL+aVZYDE4IpUk57oSFnJm4FyCqqOE0jhY2SMyLFoo56zyo6becOS5UVDdj7Vih0zp+tcMhwRpBeLyqtIjlJKAIZSbI8SGSF3k0pA3mR5tHuwPFoa7N7reoq2bqCsAk1HqCu5uvI1n6JuRXI+S1Mco54YmYTwcn6Aeic+kssXi8XpXC4V3t7/ADuTNKaQJdScAAAAAElFTkSuQmCC)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fjupyter-course%2Fsample-project.git/833ccfa0a9f33c561d4bb7cc20e2b8dae9b42052?urlpath=tree)

[![badge](https://img.shields.io/badge/launch-binder%20(JupyterLab)-F5A252.svg?logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFkAAABZCAMAAABi1XidAAAB8lBMVEX///9XmsrmZYH1olJXmsr1olJXmsrmZYH1olJXmsr1olJXmsrmZYH1olL1olJXmsr1olJXmsrmZYH1olL1olJXmsrmZYH1olJXmsr1olL1olJXmsrmZYH1olL1olJXmsrmZYH1olL1olL0nFf1olJXmsrmZYH1olJXmsq8dZb1olJXmsrmZYH1olJXmspXmspXmsr1olL1olJXmsrmZYH1olJXmsr1olL1olJXmsrmZYH1olL1olLeaIVXmsrmZYH1olL1olL1olJXmsrmZYH1olLna31Xmsr1olJXmsr1olJXmsrmZYH1olLqoVr1olJXmsr1olJXmsrmZYH1olL1olKkfaPobXvviGabgadXmsqThKuofKHmZ4Dobnr1olJXmsr1olJXmspXmsr1olJXmsrfZ4TuhWn1olL1olJXmsqBi7X1olJXmspZmslbmMhbmsdemsVfl8ZgmsNim8Jpk8F0m7R4m7F5nLB6jbh7jbiDirOEibOGnKaMhq+PnaCVg6qWg6qegKaff6WhnpKofKGtnomxeZy3noG6dZi+n3vCcpPDcpPGn3bLb4/Mb47UbIrVa4rYoGjdaIbeaIXhoWHmZYHobXvpcHjqdHXreHLroVrsfG/uhGnuh2bwj2Hxk17yl1vzmljzm1j0nlX1olL3AJXWAAAAbXRSTlMAEBAQHx8gICAuLjAwMDw9PUBAQEpQUFBXV1hgYGBkcHBwcXl8gICAgoiIkJCQlJicnJ2goKCmqK+wsLC4usDAwMjP0NDQ1NbW3Nzg4ODi5+3v8PDw8/T09PX29vb39/f5+fr7+/z8/Pz9/v7+zczCxgAABC5JREFUeAHN1ul3k0UUBvCb1CTVpmpaitAGSLSpSuKCLWpbTKNJFGlcSMAFF63iUmRccNG6gLbuxkXU66JAUef/9LSpmXnyLr3T5AO/rzl5zj137p136BISy44fKJXuGN/d19PUfYeO67Znqtf2KH33Id1psXoFdW30sPZ1sMvs2D060AHqws4FHeJojLZqnw53cmfvg+XR8mC0OEjuxrXEkX5ydeVJLVIlV0e10PXk5k7dYeHu7Cj1j+49uKg7uLU61tGLw1lq27ugQYlclHC4bgv7VQ+TAyj5Zc/UjsPvs1sd5cWryWObtvWT2EPa4rtnWW3JkpjggEpbOsPr7F7EyNewtpBIslA7p43HCsnwooXTEc3UmPmCNn5lrqTJxy6nRmcavGZVt/3Da2pD5NHvsOHJCrdc1G2r3DITpU7yic7w/7Rxnjc0kt5GC4djiv2Sz3Fb2iEZg41/ddsFDoyuYrIkmFehz0HR2thPgQqMyQYb2OtB0WxsZ3BeG3+wpRb1vzl2UYBog8FfGhttFKjtAclnZYrRo9ryG9uG/FZQU4AEg8ZE9LjGMzTmqKXPLnlWVnIlQQTvxJf8ip7VgjZjyVPrjw1te5otM7RmP7xm+sK2Gv9I8Gi++BRbEkR9EBw8zRUcKxwp73xkaLiqQb+kGduJTNHG72zcW9LoJgqQxpP3/Tj//c3yB0tqzaml05/+orHLksVO+95kX7/7qgJvnjlrfr2Ggsyx0eoy9uPzN5SPd86aXggOsEKW2Prz7du3VID3/tzs/sSRs2w7ovVHKtjrX2pd7ZMlTxAYfBAL9jiDwfLkq55Tm7ifhMlTGPyCAs7RFRhn47JnlcB9RM5T97ASuZXIcVNuUDIndpDbdsfrqsOppeXl5Y+XVKdjFCTh+zGaVuj0d9zy05PPK3QzBamxdwtTCrzyg/2Rvf2EstUjordGwa/kx9mSJLr8mLLtCW8HHGJc2R5hS219IiF6PnTusOqcMl57gm0Z8kanKMAQg0qSyuZfn7zItsbGyO9QlnxY0eCuD1XL2ys/MsrQhltE7Ug0uFOzufJFE2PxBo/YAx8XPPdDwWN0MrDRYIZF0mSMKCNHgaIVFoBbNoLJ7tEQDKxGF0kcLQimojCZopv0OkNOyWCCg9XMVAi7ARJzQdM2QUh0gmBozjc3Skg6dSBRqDGYSUOu66Zg+I2fNZs/M3/f/Grl/XnyF1Gw3VKCez0PN5IUfFLqvgUN4C0qNqYs5YhPL+aVZYDE4IpUk57oSFnJm4FyCqqOE0jhY2SMyLFoo56zyo6becOS5UVDdj7Vih0zp+tcMhwRpBeLyqtIjlJKAIZSbI8SGSF3k0pA3mR5tHuwPFoa7N7reoq2bqCsAk1HqCu5uvI1n6JuRXI+S1Mco54YmYTwcn6Aeic+kssXi8XpXC4V3t7/ADuTNKaQJdScAAAAAElFTkSuQmCC)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fjupyter-course%2Fsample-project.git/833ccfa0a9f33c561d4bb7cc20e2b8dae9b42052?urlpath=lab)

Note that the interactive figure in the data exploration notebook currently only works in the classic notebook (at least on binder).
Launching binder may take a moment.

If you want to install the project and keep your modifications of the contents, read on.

## Dependencies

I assume you install and run the project on GWDG's JupyterHub service, [https://jupyter-cloud.gwdg.de](https://jupyter-cloud.gwdg.de), which comes with basic dependencies pre-installed and runs JupyterLab (instead of Jupyter Notebook). If you prefer your own comupter, you have to install Python and JupyterLab locally, e.g. by downloading Python from https://www.python.org/downloads/ and [installing JupyterLab via pip](https://jupyterlab.readthedocs.io/en/latest/getting_started/installation.html#installation). You will further have to install:

- [Git](https://git-scm.com/downloads)
- [`ipywidgets` extension for JupyterLab](https://ipywidgets.readthedocs.io/en/7.6.3/user_install.html)
- [`bokeh` extension for JupyterLab](https://docs.bokeh.org/en/latest/docs/user_guide/jupyter.html?highlight=jupyterlab#jupyterlab)
- [pandoc](https://pandoc.org/installing.html) (required only for document conversion)

## Downloading the project

Clone the project *via* git.
Type in the terminal (command line):

```bash
git clone https://gitlab.gwdg.de/jupyter-course/sample-project.git --depth=1
```

You can also [download the project as a zip file](https://gitlab.gwdg.de/jupyter-course/sample-project/-/archive/master/sample-project-master.zip) and extract it to the location of your choice.

Enter the newly created directory:

```bash
cd sample-project
```

## Installing the environment

We will use a dedicated environment for our project, as we should for every project.
The sample project includes a `requirements.txt` file which lists all the Python
packages and versions that we need to install to run the code of the project.
The installation process is layed out in the following.
Again, the commands are to be typed into the terminal (command line).
Executing some of these commands, especially `pip install -r requirements.txt` can take a while, be patient ...

Make a dedicated Python environment:

```bash
python3 -m venv env
```

Activate the environment:

```bash
source env/bin/activate
```

(Note that `(env)` will now be displayed at the beginning of the command prompt,
indicating that the environment *env* is active.)

Update `pip` and install `wheel`, a tool that helps installing other packages:

```bash
pip install -U pip wheel
```

Install the required packages:

```bash
pip install -r requirements.txt
```

Register a new Python kernel with Jupyter:

```bash
python -m ipykernel install --name sample-project --user
```

## Testing the project

To test that your installation runs as expected, run:

```bash
python test/test_data.py
python test/test_notebooks.py
```

If everything is fine, you should see the message

```
.
----------------------------------------------------------------------
Ran 1 test in x.y s

OK
```

after running each of the test scripts.
The second test script (`test_notebooks.py`) runs *all notebooks* of the project,
which takes a little while.

## Uninstalling the environment

You can remove the project's environment as follows:


Delete the `env` folder inside project directory:

```bash
rm -r env
```

This takes quite long, because MANY small files have to be deleted.
You can see which files are deleted if you add the `-v` flag to the `rm` command:

```bash
rm -rv env
```

Uninstall the Jupyter kernel:

```bash
jupyter kernelspec remove sample-project
```

## Converting documents

To convert documents, you can simply click *File*  *Download as*
(Jupyter notebook) or *File*  *Export Notebook As ...* (JupyterLab).
This will run the tool [nbconvert](https://nbconvert.readthedocs.io/en/latest/)
to produce the file that you want (depending on the export format, more dependencies may need to be installed).

You have more control over the process if you run `nbconvert` in the terminal.
For example, you can suppress code input cells with the `--no-input` flag:

```bash
jupyter nbconvert 4_report.ipynb --to=html --no-input
```

A good starting point to convert to docx or tex is Markdown:

```bash
jupyter nbconvert 4_report.ipynb --to=markdown --no-input
```

Subsequently, you can use [pandoc](https://pandoc.org) to create a Word
document ...

```bash
pandoc -i 4_report.md -o 4_report.docx
```

... or a LaTeX document:

```bash
pandoc -i 4_report.md -o 4_report.tex
```

Both variants will need some touch up.
You can find results of some conversions in the folder `notebooks/converted`.
